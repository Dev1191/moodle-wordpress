<?php
/*
  Template Name: ThankYou

 */

get_header();
?>
<style>
    .attempt-btn{
        margin:480px;
    }

    @media only screen and (min-width: 0px) and (max-width: 768px) {
        .attempt-btn{
            margin:0px;
        }
    }
</style>
<div class="wrap">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php
            global $current_user, $wpdb;
            get_currentuserinfo();
            if ($current_user->ID != 0) {
                $userid = $current_user->ID;
                $orderid = $_REQUEST['order'];
                $sqldata = $wpdb->get_row("	
				SELECT * 
				FROM  wp_users
					WHERE id = " . $userid
                );
                $username = $sqldata->user_login;
                $useremail = $sqldata->user_email;
                $userfullname = $sqldata->display_name;
                $firstname = $wpdb->get_row("SELECT meta_value FROM `wp_usermeta` WHERE user_id = '$userid' AND meta_key = 'billing_first_name' ");
                $lastname = $wpdb->get_row(" SELECT meta_value FROM `wp_usermeta` WHERE user_id = '$userid' AND meta_key = 'billing_last_name' ");
                $fname = $firstname->meta_value;
                $lname = $lastname->meta_value;
            } else {
                $orderid = $_REQUEST['order'];
                $order = new WC_Order($orderid);
                $useremail = $order->billing_email;
                $userfullname = $order->billing_first_name . ' ' . $order->billing_last_name;
                $username = $order->billing_first_name;
                $fname = $order->billing_first_name;
                $lname = $order->billing_last_name;
            }

            $results = $wpdb->get_results("
				SELECT * 
				FROM  wp_woocommerce_order_items
					WHERE order_id = '" . $orderid . "'
			 ");
            $count = count($results);
            if ($count > 1) {
                $result = $wpdb->get_results("
				SELECT * 
				FROM  wp_woocommerce_order_items
					WHERE order_id = '" . $orderid . "' ORDER BY order_item_id ASC LIMIT 1,10"
                );
            } else {
                $result = $wpdb->get_results("
				SELECT * 
				FROM  wp_woocommerce_order_items
					WHERE order_id = '" . $orderid . "' "
                );
            }


            $coursearray = array();
            foreach ($result as $data) {
                array_push($coursearray, $data->order_item_id);
            }
            $implode = implode(",", $coursearray);
            $productids = $wpdb->get_results("
				SELECT * 
				FROM  wp_woocommerce_order_itemmeta
					WHERE order_item_id IN($implode) AND meta_key='_product_id'
			 ");
            $productarr = array();

            foreach ($productids as $productid) {
                array_push($productarr, $productid->meta_value);
            }
            $implodepr = implode(",", $productarr);
            $courseids = $wpdb->get_results("
				SELECT * 
				FROM  wp_wootomoodle
					WHERE productid IN($implodepr)
			 ");
            //print_r($productids);
            $coursearr = array();
            foreach ($courseids as $courseid) {
                array_push($coursearr, $courseid->courseid);
            }
            $implodecr = implode(",", $coursearr);
            $wpdb->insert(
                    'wp_custom_cron', array(
                'orderid' => $orderid,
                'userid' => $userid,
                'createddate' => time(),
                'status' => 0
                    ), array(
                '%d',
                '%d',
                '%d',
                '%d'
                    )
            );

            function getassigntime($orderid) {
                global $wpdb;
                $order = wc_get_order($orderid);
                $items = $order->get_items();
                foreach ($items as $item) {
                    $product_id = $item->get_product_id();
                    break;
                }
                $checksubs = $wpdb->get_row("SELECT * FROM `wp_subscription_details` WHERE product_id = '$product_id'");
                if (!empty($checksubs)) {
                    return $_SESSION['noofmonth'];
                    //$_SESSION['p_custom_price'];
                } else {
                    return 0;
                }
            }

            $months = getassigntime($orderid);
            $table_name = $wpdb->prefix . "wooadminmoodle";
            $getmoodle = $wpdb->get_row("SELECT * FROM  $table_name ORDER BY id DESC LIMIT 1 ");
            $murl = $getmoodle->hosturl;
            ?>
            <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-xs-12 col-sm-12 text-center">
                        <div class="attempt-quiz">

                            <?php
                            $order = new WC_Order($orderid);
                            $total = $order->total;
                            $discount = $order->discount_total;
                            $payment_method = $order->payment_method;
                            $transaction_id = $order->transaction_id;
                            $status = $order->status;

                            foreach ($results as $course) {
                                $coursename = $course->order_item_name;
                            }
                            ?>
                            <h3 class="page-header text-center">Your Purchase Order</h3>
                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <tr>
                                    <th>
                                        <b>Product</b>
                                    </th>
                                    <th><b>Total</b></th>
                                </tr>
                                <tr>
                                    <td><b>Transaction ID:</b> </td>
                                    <td><?php echo $transaction_id; ?></td>
                                </tr>
                                <tr>
                                    <td><b><?php echo $coursename; ?></b> </td>
                                    <td><?php echo $total; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Status:</b> </td>
                                    <td><?php echo $status; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Subtotal:</b> </td>
                                    <td><?php echo $total; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Payment method:</b> </td>
                                    <td><?php echo $payment_method; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Total:</b> </td>
                                    <td><?php echo $total; ?></td>
                                </tr>
                            </table>
                            <button class="btn btn-primary col-md-4 col-md-offset-6" onclick="attemptQuiz()">Start Attempt Quiz</button>
                        </div>
                    </div>
                </div>
            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script>
                              function attemptQuiz() {
                                        var user = "<?php echo $username; ?>";
                                        var email = "<?php echo $useremail; ?>";
                                        var course = "<?php echo $implodecr; ?>";
                                        var displayname = "<?php echo $userfullname; ?>";
                                        var firstname = "<?php echo $fname; ?>";
                                        var lastname = "<?php echo $lname; ?>";
                                        var months = "<?php echo $months; ?>";
                                        var murl = "<?php echo $murl; ?>";
                                        window.location.href = murl + "/m32/login/index.php?user=" + user + "&email=" + email + "&course=" + course + "&name=" + displayname + "&firstname=" + firstname + "&lastname=" + lastname + "&months=" + months;
                                    }
                                

            </script>

        </main>
    </div>
</div>

<?php get_footer(); ?>