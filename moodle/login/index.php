<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Main login page.
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../config.php');
require_once('lib.php');
global $DB, $CFG;
//print_r($_REQUEST);die;
// Array ( [user] => santosh [email] => santosh@ldsengineers.com [course] => 53 [name] => santosh kumar [firstname] => santosh [lastname] => kumar [_ga] => GA1.2.1995299567.1511335803 [_gid] => GA1.2.1006967
// url == user=mikerayan&email=mikerayan@gmail.com&course=6&name=mikerayan&firstname=mikerayan&lastname=&months=12



if ((!empty($_REQUEST['user'])) && (!empty($_REQUEST['email']))) {
    $months=$_REQUEST['months'];
    if($months >0 ){
        $today = time();
$assignend = strtotime("+$months months", $today);
    }else{
        $assignend=0;
    }
    if ($_REQUEST['course'] != '') {
        //$course = $_REQUEST['course'];
        $coursename = explode(',', $_REQUEST['course']);
        $courseimplode = "'" . implode("', '", $coursename) . "'";
        $courseid = $DB->get_records_sql("SELECT id  FROM {course} WHERE id IN ($courseimplode)");
        $temparr = array();
        foreach ($courseid as $ids) {
            array_push($temparr, $ids->id);
        }
        $course = implode(',', $temparr);
        //print_r($course);die;
    }
    if ($_REQUEST['bundle'] != '') {
        $bundles = $_REQUEST['bundle'];
        $bundleids = $DB->get_records_sql("SELECT * FROM {course_bundle} WHERE id IN($bundles)");
        if ($bundleids) {
            $arrbndl = array();
            foreach ($bundleids as $bundleidsdata) {
                array_push($arrbndl, $bundleidsdata->id);
            }
            if (!empty($arrbndl)) {
                $impbndlid = implode(',', $arrbndl);
                $courseids = $DB->get_records_sql("SELECT * FROM {course_bundle} WHERE id IN($impbndlid)");
                $arrcurseid = array();
                foreach ($courseids as $courseidsdata) {
                    array_push($arrcurseid, $courseidsdata->course);
                }
                if (!empty($arrcurseid)) {
                    $course = implode(',', $arrcurseid);
                }
            }
        }
    }
    $userrole = 5;

    function enrolCourse($courseid, $userid, $userrole,$assignend) {

        $coursebandle = explode(',', $courseid);
        $totalcourse = count($coursebandle);

        for ($i = 0; $i <= $totalcourse - 1; $i++) {

            global $DB, $CFG;
            $query = "SELECT * FROM {enrol} WHERE enrol = 'manual' AND courseid = $coursebandle[$i] ";
            $enrollmentID = $DB->get_record_sql($query);

            if (!empty($enrollmentID->id)) {

                if (!$DB->record_exists('user_enrolments', array('enrolid' => $enrollmentID->id, 'userid' => $userid))) {

                    //echo "sdfsdfsdfs". $userid."dfsdfsd". $enrollmentID->id;die;
                    $userenrol = new stdClass();
                    $userenrol->status = 0;
                    $userenrol->userid = $userid;
                    $userenrol->enrolid = $enrollmentID->id;
                    $userenrol->timestart = time();
                    $userenrol->timeend = $assignend;
                    $userenrol->modifierid = 2;
                    $userenrol->timecreated = time();
                    $userenrol->timemodified = time();
                    $enrol_manual = enrol_get_plugin('manual');
                    $enrol_manual->enrol_user($enrollmentID, $userid, $userrole, $userenrol->timestart, $userenrol->timeend);
                    add_to_log($courseid, 'course', 'enrol', '../enrol/users.php?id=' . $courseid, $courseid, $userid); //there should be userid somewhere!
                    //$mappedcourse =  new stdClass();
                    //$mappedcourse->enrolled = 1;
                    //$DB->update_record('course_user_mapping',$mappedcourse,array('userid'=>$userid));
                    //  $sql = "UPDATE {course_user_mapping} SET  enrolled = 1 where userid=".$userid;
                    //$DB->execute($sql);
                    // $DB->execute($sql, array $parms=null)
                }
            }
        }
    }

    $useremail = $_REQUEST['email'];
    $firstname = $_REQUEST['firstname'];
    $lastname = $_REQUEST['lastname'];

    $checkusername = $DB->get_record_sql("SELECT * FROM {user} WHERE username = '" . $_REQUEST['user'] . "'");
    if ($checkusername) {
        $username = $_REQUEST['user'] . rand(1, 100);
    } else {
        $username = $_REQUEST['user'];
    }
    $insertdata = new stdClass();
    $insertdata->confirmed = 1;
    $insertdata->timecreated = time();
    $insertdata->mnethostid = $CFG->mnet_localhost_id;
    $insertdata->username = trim($username);
    $insertdata->password = md5("P@ssw0rd");
    $insertdata->firstname = $firstname;
    $insertdata->lastname = $lastname;
    $insertdata->email = $useremail;
    $insertdata->auth = 'manual';

//echo "SELECT * FROM {user} WHERE email LIKE  '%" . $insertdata->email . "%'";
    $records = $DB->get_record_sql("SELECT * FROM {user} WHERE email LIKE  '%" . $insertdata->email . "%'");
    if (empty($records)) {
        // echo 1111111;die;
        // first time user if not in user list
        $userid = $DB->insert_record('user', $insertdata);
        if ($userid) {
            $emailuser = new stdClass();
            //$emailuser->email = 'sushil@ldsengineers.com';
            $emailuser->email = $insertdata->email;
            $emailuser->maildisplay = true;
            $emailuser->mailformat = 1; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
            $emailuser->id = -1;
            $emailuser->firstnamephonetic = false;
            $emailuser->lastnamephonetic = false;
            $emailuser->middlename = false;
            $emailuser->alternatename = false;
            $subject = 'Login information for French course at Language City';
            $fromUser = 'Alexandre de Chambure';
            $message = $insertdata->firstname . ',
                                    
Thank you for registering for our course at Language City. To access your course, go to https://moodle.language-city.com/. Your user name is ' . $insertdata->username . ', and your password is P@ssw0rd. Feel free to use the forums located below each lesson & exercise to ask me questions or communicate with other students. Of course, you can also email me at alex@language-city.com. Enjoy the course!

Best,

Alexandre de Chambure
';
            $mail = email_to_user($emailuser, $fromUser, $subject, $message, $messagehtml = '', $attachment = '', $attachname = '', $usetrueaddress = true, $replyto = '', $replytoname = '', $wordwrapwidth = 70);
        }
        // echo "new user".$userid;die;
        // insert data in user mapping table for check enrollment
        // this is also create for cron 
        // if user is not enroll through this page. cron will be enroll user in particular test.

        $insertcoursemapping = new stdClass();
        $insertcoursemapping->courseid = $course;
        $insertcoursemapping->userid = $userid;
        $insertcoursemapping->enrolled = 0;
        $insertcoursemapping->timecreated = time();

        //  $DB->insert_record('course_user_mapping', $insertcoursemapping);

        if (!empty($userid)) {
            enrolCourse($course, $userid, $userrole, $assignend);
        }
    } else {
        // echo 322222;die;
        enrolCourse($course, $records->id, $userrole, $assignend);
    }
}

//http://59.144.171.89/m32/login/index.php?user_login=dean11&user_email=dean11@gmail.com&courseid=5
if(!empty($_REQUEST['user_email']) || !empty($_REQUEST['user_login']) || !empty($_REQUEST['courseid']) ){

    $login_email = $_REQUEST['user_email'];
    $getluser = $DB->get_record('user', array('email'=>$login_email), $fields='*');
    $luser = $getluser->username;
}

// Try to prevent searching for sites that allow sign-up.
if (!isset($CFG->additionalhtmlhead)) {
    $CFG->additionalhtmlhead = '';
}
$CFG->additionalhtmlhead .= '<meta name="robots" content="noindex" />';

redirect_if_major_upgrade_required();

$testsession = optional_param('testsession', 0, PARAM_INT); // test session works properly
$cancel      = optional_param('cancel', 0, PARAM_BOOL);      // redirect to frontpage, needed for loginhttps
$anchor      = optional_param('anchor', '', PARAM_RAW);      // Used to restore hash anchor to wantsurl.
$mdcourse = optional_param('course', 0, PARAM_INT); 
$mdlogincourse = optional_param('courseid', 0, PARAM_INT); 
if ($cancel) {
    redirect(new moodle_url('/'));
}

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

$context = context_system::instance();
$PAGE->set_url("$CFG->httpswwwroot/login/index.php");
$PAGE->set_context($context);
$PAGE->set_pagelayout('login');

/// Initialize variables
$errormsg = '';
$errorcode = 0;

// login page requested session test
if ($testsession) {
    if ($testsession == $USER->id) {
        if (isset($SESSION->wantsurl)) {
            $urltogo = $SESSION->wantsurl;
        } else {
            $urltogo = $CFG->wwwroot.'/hello';
        }
        unset($SESSION->wantsurl);
        redirect($urltogo);
    } else {
        // TODO: try to find out what is the exact reason why sessions do not work
        $errormsg = get_string("cookiesnotenabled");
        $errorcode = 1;
    }
}

/// Check for timed out sessions
if (!empty($SESSION->has_timed_out)) {
    $session_has_timed_out = true;
    unset($SESSION->has_timed_out);
} else {
    $session_has_timed_out = false;
}

/// auth plugins may override these - SSO anyone?
$frm  = false;
$user = false;

$authsequence = get_enabled_auth_plugins(true); // auths, in sequence
foreach($authsequence as $authname) {
    $authplugin = get_auth_plugin($authname);
    $authplugin->loginpage_hook();
}


/// Define variables used in page
$site = get_site();

// Ignore any active pages in the navigation/settings.
// We do this because there won't be an active page there, and by ignoring the active pages the
// navigation and settings won't be initialised unless something else needs them.
$PAGE->navbar->ignore_active();
$loginsite = get_string("loginsite");
$PAGE->navbar->add($loginsite);

if ($user !== false or $frm !== false or $errormsg !== '') {
    // some auth plugin already supplied full user, fake form data or prevented user login with error message

} else if (!empty($SESSION->wantsurl) && file_exists($CFG->dirroot.'/login/weblinkauth.php')) {
    // Handles the case of another Moodle site linking into a page on this site
    //TODO: move weblink into own auth plugin
    include($CFG->dirroot.'/login/weblinkauth.php');
    if (function_exists('weblink_auth')) {
        $user = weblink_auth($SESSION->wantsurl);
    }
    if ($user) {
        $frm->username = $user->username;
    } else {
        $frm = data_submitted();
    }

} else {
    $frm = data_submitted();

    if (isset($_REQUEST['user'])) {
        $frm->username = $_REQUEST['user'];
        $frm->password= 'P@ssw0rd'; // enable for second time login 
    }

    if(isset($_REQUEST['user_login'])){
       $frm->username =  $luser;
        $frm->password= 'P@ssw0rd'; // enable for second time login 
    }


}

// Restore the #anchor to the original wantsurl. Note that this
// will only work for internal auth plugins, SSO plugins such as
// SAML / CAS / OIDC will have to handle this correctly directly.
if ($anchor && isset($SESSION->wantsurl) && strpos($SESSION->wantsurl, '#') === false) {
    $wantsurl = new moodle_url($SESSION->wantsurl);
    $wantsurl->set_anchor(substr($anchor, 1));
    $SESSION->wantsurl = $wantsurl->out();
}



if (isset($frm->username)) {

    $rsLastaccess = $DB->get_record_sql(" SELECT * FROM {user} WHERE lastaccess <> '0' AND username = '$frm->username'");
    //  echo "<pre>sasa";
    //print_r($rsLastaccess);
}

if (strcmp(trim($frm->username), 'admin') != 0 && $rsLastaccess->lastaccess == intval(0)) {

    if (isset($_REQUEST['user'])) {
        $frm->password = 'P@ssw0rd';
    }
    
}

if (strcmp(trim($frm->username), 'guest') == 0) {
    $frm->password = 'guest';
}


/// Check if the user has actually submitted login data to us

if ($frm and isset($frm->username)) {                             // Login WITH cookies



    $frm->username = trim(core_text::strtolower($frm->username));

    if (is_enabled_auth('none') ) {
        if ($frm->username !== core_user::clean_field($frm->username, 'username')) {
            $errormsg = get_string('username').': '.get_string("invalidusername");
            $errorcode = 2;
            $user = null;
        }
    }

    if ($user) {
        //user already supplied by aut plugin prelogin hook
    } else if (($frm->username == 'guest') and empty($CFG->guestloginbutton)) {
        $user = false;    /// Can't log in as guest if guest button is disabled
        $frm = false;
    } else {
        if (empty($errormsg)) {
            $user = authenticate_user_login($frm->username, $frm->password, false, $errorcode);
        }
    }

    // Intercept 'restored' users to provide them with info & reset password
    if (!$user and $frm and is_restored_user($frm->username)) {
        $PAGE->set_title(get_string('restoredaccount'));
        $PAGE->set_heading($site->fullname);
        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('restoredaccount'));
        echo $OUTPUT->box(get_string('restoredaccountinfo'), 'generalbox boxaligncenter');
        require_once('restored_password_form.php'); // Use our "supplanter" login_forgot_password_form. MDL-20846
        $form = new login_forgot_password_form('forgot_password.php', array('username' => $frm->username));
        $form->display();
        echo $OUTPUT->footer();
        die;
    }

    if ($user) {

        // language setup
        if (isguestuser($user)) {
            // no predefined language for guests - use existing session or default site lang
            unset($user->lang);

        } else if (!empty($user->lang)) {
            // unset previous session language - use user preference instead
            unset($SESSION->lang);
        }

        if (empty($user->confirmed)) {       // This account was never confirmed
            $PAGE->set_title(get_string("mustconfirm"));
            $PAGE->set_heading($site->fullname);
            echo $OUTPUT->header();
            echo $OUTPUT->heading(get_string("mustconfirm"));
            echo $OUTPUT->box(get_string("emailconfirmsent", "", $user->email), "generalbox boxaligncenter");
            echo $OUTPUT->footer();
            die;
        }

    /// Let's get them all set up.
        complete_user_login($user);

        \core\session\manager::apply_concurrent_login_limit($user->id, session_id());

        // sets the username cookie
        if (!empty($CFG->nolastloggedin)) {
            // do not store last logged in user in cookie
            // auth plugins can temporarily override this from loginpage_hook()
            // do not save $CFG->nolastloggedin in database!

        } else if (empty($CFG->rememberusername) or ($CFG->rememberusername == 2 and empty($frm->rememberusername))) {
            // no permanent cookies, delete old one if exists
            set_moodle_cookie('');

        } else {
            set_moodle_cookie($USER->username);
        }

        // for user login first time and login for second time
     if(!empty($mdcourse)){
        $getcourse_module =  $DB->get_record('course_modules', array('module'=>'16','course'=>$mdcourse), $fields='*'); 
     }
     else if(!empty($mdlogincourse)){
        $getcourse_module =  $DB->get_record('course_modules', array('module'=>'16','course'=>$mdlogincourse), $fields='*'); 
     }


    if($getcourse_module){
          $urltogo = $CFG->wwwroot.'/mod/quiz/view.php?id='.$getcourse_module->id;
    }else{
           $urltogo = core_login_get_return_url();
    }


    /// check if user password has expired
    /// Currently supported only for ldap-authentication module
        $userauth = get_auth_plugin($USER->auth);
        if (!isguestuser() and !empty($userauth->config->expiration) and $userauth->config->expiration == 1) {
            $externalchangepassword = false;
            if ($userauth->can_change_password()) {
                $passwordchangeurl = $userauth->change_password_url();
                if (!$passwordchangeurl) {
                    $passwordchangeurl = $CFG->httpswwwroot.'/login/change_password.php';
                } else {
                    $externalchangepassword = true;
                }
            } else {
                $passwordchangeurl = $CFG->httpswwwroot.'/login/change_password.php';
            }
            $days2expire = $userauth->password_expire($USER->username);
            $PAGE->set_title("$site->fullname: $loginsite");
            $PAGE->set_heading("$site->fullname");
            if (intval($days2expire) > 0 && intval($days2expire) < intval($userauth->config->expiration_warning)) {
                echo $OUTPUT->header();
                echo $OUTPUT->confirm(get_string('auth_passwordwillexpire', 'auth', $days2expire), $passwordchangeurl, $urltogo);
                echo $OUTPUT->footer();
                exit;
            } elseif (intval($days2expire) < 0 ) {
                if ($externalchangepassword) {
                    // We end the session if the change password form is external. This prevents access to the site
                    // until the password is correctly changed.
                    require_logout();
                } else {
                    // If we use the standard change password form, this user preference will be reset when the password
                    // is changed. Until then it will prevent access to the site.
                    set_user_preference('auth_forcepasswordchange', 1, $USER);
                }
                echo $OUTPUT->header();
                echo $OUTPUT->confirm(get_string('auth_passwordisexpired', 'auth'), $passwordchangeurl, $urltogo);
                echo $OUTPUT->footer();
                exit;
            }
        }

        // Discard any errors before the last redirect.
        unset($SESSION->loginerrormsg);

        // test the session actually works by redirecting to self
        $SESSION->wantsurl = $urltogo;
        redirect(new moodle_url(get_login_url(), array('testsession'=>$USER->id)));

    } else {
        if (empty($errormsg)) {
            if ($errorcode == AUTH_LOGIN_UNAUTHORISED) {
                $errormsg = get_string("unauthorisedlogin", "", $frm->username);
            } else {
                $errormsg = get_string("invalidlogin");
                $errorcode = 3;
            }
        }
    }
}

/// Detect problems with timedout sessions
if ($session_has_timed_out and !data_submitted()) {
    $errormsg = get_string('sessionerroruser', 'error');
    $errorcode = 4;
}

/// First, let's remember where the user was trying to get to before they got here

if (empty($SESSION->wantsurl)) {
    $SESSION->wantsurl = null;
    $referer = get_local_referer(false);
    if ($referer &&
            $referer != $CFG->wwwroot &&
            $referer != $CFG->wwwroot . '/' &&
            $referer != $CFG->httpswwwroot . '/login/' &&
            strpos($referer, $CFG->httpswwwroot . '/login/?') !== 0 &&
            strpos($referer, $CFG->httpswwwroot . '/login/index.php') !== 0) { // There might be some extra params such as ?lang=.
        $SESSION->wantsurl = $referer;
    }
}

/// Redirect to alternative login URL if needed
if (!empty($CFG->alternateloginurl)) {
    $loginurl = new moodle_url($CFG->alternateloginurl);

    $loginurlstr = $loginurl->out(false);

    if (strpos($SESSION->wantsurl, $loginurlstr) === 0) {
        // We do not want to return to alternate url.
        $SESSION->wantsurl = null;
    }

    // If error code then add that to url.
    if ($errorcode) {
        $loginurl->param('errorcode', $errorcode);
    }

    redirect($loginurl->out(false));
}

// make sure we really are on the https page when https login required
$PAGE->verify_https_required();

/// Generate the login page with forms

if (!isset($frm) or !is_object($frm)) {
    $frm = new stdClass();
}

if (empty($frm->username) && $authsequence[0] != 'shibboleth') {  // See bug 5184
    if (!empty($_GET["username"])) {
        // we do not want data from _POST here
        $frm->username = clean_param($_GET["username"], PARAM_RAW); // we do not want data from _POST here
    } else {
        $frm->username = get_moodle_cookie();
    }

    $frm->password = "";
}

if (!empty($SESSION->loginerrormsg)) {
    // We had some errors before redirect, show them now.
    $errormsg = $SESSION->loginerrormsg;
    unset($SESSION->loginerrormsg);

} else if ($testsession) {
    // No need to redirect here.
    unset($SESSION->loginerrormsg);

} else if ($errormsg or !empty($frm->password)) {
    // We must redirect after every password submission.
    if ($errormsg) {
        $SESSION->loginerrormsg = $errormsg;
    }
    redirect(new moodle_url($CFG->httpswwwroot . '/login/index.php'));
}

$PAGE->set_title("$site->fullname: $loginsite");
$PAGE->set_heading("$site->fullname");

echo $OUTPUT->header();

if (isloggedin() and !isguestuser()) {
    // prevent logging when already logged in, we do not want them to relogin by accident because sesskey would be changed
    echo $OUTPUT->box_start();
    $logout = new single_button(new moodle_url($CFG->httpswwwroot.'/login/logout.php', array('sesskey'=>sesskey(),'loginpage'=>1)), get_string('logout'), 'post');
    $continue = new single_button(new moodle_url($CFG->httpswwwroot.'/login/index.php', array('cancel'=>1)), get_string('cancel'), 'get');
    echo $OUTPUT->confirm(get_string('alreadyloggedin', 'error', fullname($USER)), $logout, $continue);
    echo $OUTPUT->box_end();
} else {
    $loginform = new \core_auth\output\login($authsequence, $frm->username);
    $loginform->set_error($errormsg);
    echo $OUTPUT->render($loginform);
}

echo $OUTPUT->footer();
