<?php
//-----define( 'WP_DEBUG', true );
require_once(dirname(dirname(__FILE__)) . '/wp-load.php');
include(ABSPATH . 'wp-admin/admin-header.php');
auth_redirect();



if(isset($_REQUEST['save'])){
global $wpdb;

$cdate=date('Y-m-d h:i:s');
$hosturl = $_REQUEST['hosturl'];
$servername = $_REQUEST['servername'];
$username = $_REQUEST['username'];
$password = $_REQUEST['password'];
$dbname = $_REQUEST['databasename'];


 $table_name = $wpdb->prefix . "wooadminmoodle";
  $res = $wpdb->insert($table_name, array('hosturl'=>$hosturl,'server' => $servername,'username' =>$username,'password' => $password,'dbname'=>$dbname,'datecreated' =>$cdate));  
  
  if($res){
  	echo 'inserted';
  }else{
  	echo 'not inserted';
  }

}

?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">

<div class="row">
    <div class="col-md-8">

<h2>Course Moodle Connect </h2>
<br>
<br>
<form action="" name="moodleconnect">
<table  id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<tr>
		<td><label>Host Url</label></td>
		<td><input type="text" name="hosturl" class="form-control" placeholder="Host Url"></td>
	</tr>
	<tr>
		<td><label>Server Name</label></td>
		<td><input type="text" name="servername" class="form-control" placeholder="Server Name"></td>
	</tr>
	<tr>
		<td><label>User Name</label></td>
		<td><input type="text" name="username" class="form-control" placeholder="User Name"></td>
	</tr>
		<tr>
		<td><label>Password</label></td>
		<td><input type="password" name="password" class="form-control" placeholder="Password"></td>
	</tr>
		</tr>
		<tr>
		<td><label>Database Name</label></td>
		<td><input type="text" name="databasename" class="form-control" placeholder="Database Name"></td>
	</tr>
	<tr>
		<td colspan="2">
		    <button type="submit" class="btn btn-success pull-right" name="save" value="Save">Submit</button>
		</td>
	</tr>

</table>
</form>
</hr>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>S No.</th>
                <th>Host Url</th>
                <th>Server Name</th>
                <th>User Name</th>
                <th>Database Name</th>
                <th>Added Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>S No.</th>
                <th>Host Url</th>
                <th>Server Name</th>
                <th>User Name</th>
                <th>Database Name</th>
                <th>Added Date</th>
                <th>Actions</th>
            </tr>
        </tfoot>
        <tbody>
        	            <?php $myrows = $wpdb->get_results( "SELECT * FROM wp_wooadminmoodle");
            $i=1;			
 foreach ($myrows as $myrowsdata){



    ?>
        <tr>
                <td><?php echo $i.'.';?></td>
                <td><?php echo  $myrowsdata->hosturl; ?></td>
                <td><?php echo  $myrowsdata->server; ?></td>
                <td><?php echo $myrowsdata->username;; ?></td>
                <td><?php echo $myrowsdata->dbname;; ?></td>
                <td><?php echo date('d/m/Y h:i:s',strtotime($myrowsdata->datecreated));?></td>
                <td><a class="btn btn-info" href="wooadminmoodleedit.php?id=<?php echo $myrowsdata->id ;?>">Edit</a>&nbsp;<a class="btn btn-danger" onclick="wooadminmoodledelete(<?php echo $myrowsdata->id ;?>)">Delete</a></td>
            </tr><?php 
$i++; }
            ?>
        </tbody>
    </table>

</div>
</div>

<script src="//code.jquery.com/jquery-1.12.4.js"> </script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"> </script>
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
function wooadminmoodledelete(id){
var txt;
var r = confirm("Do you want to delete this record!");
if (r == true) {
    window.location.href="wooadminmoodledelete.php?id="+id;
} else {
   return false;
}
}
  </script>
    <?php 
						
						include(ABSPATH . 'wp-admin/admin-footer.php');
						?>
