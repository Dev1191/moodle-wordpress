<?php
ob_start();
//define( 'WP_DEBUG', true );
require_once(dirname(dirname(__FILE__)) . '/wp-load.php');
include(ABSPATH . 'wp-admin/admin-header.php');
auth_redirect();
global $wpdb;

$id = $_REQUEST['id'];

$getmoodle = $wpdb->get_row("SELECT * FROM wp_wooadminmoodle WHERE id= $id");

if(isset($_REQUEST['update'])){

$cdate=date('Y-m-d h:i:s');
$ids = $_REQUEST['ids'];
$hosturl = $_REQUEST['hosturl'];
$servername = $_REQUEST['servername'];
$username = $_REQUEST['username'];
$password = $_REQUEST['password'];
 $dbname = $_REQUEST['databasename'];

  $table_name = $wpdb->prefix . "wooadminmoodle";

$res = $wpdb->update($table_name, array('hosturl'=>$hosturl,'server' => $servername,'username' =>$username,'password' => $password,'dbname'=>$dbname,'datecreated' =>$cdate),array('id'=>$ids),array('%s','%s','%s','%s','%s','%s'),array('%s'));

if($res){
  wp_redirect(admin_url().'wooadminmoodle.php');
}
else{

	echo 'not updated';
}

}
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-8">

<h2>Course Moodle Connect </h2>
<br>
<br>
<form action="" name="moodleconnect">
<table  id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<tr>
		<td><label>Host Url</label></td>
		<td>
			<input type="hidden" name="ids" value="<?php echo $getmoodle->id; ?>">
			<input type="text" name="hosturl" class="form-control" placeholder="Host Url" value="<?php echo $getmoodle->hosturl; ?>"></td>
	</tr>
	<tr>
		<td><label>Server Name</label></td>
		<td><input type="text" name="servername" class="form-control" placeholder="Server Name" value="<?php echo $getmoodle->server;?>"></td>
	</tr>
	<tr>
		<td><label>User Name</label></td>
		<td><input type="text" name="username" class="form-control" placeholder="User Name" value="<?php echo $getmoodle->username;;?>"></td>
	</tr>
		<tr>
		<td><label>Password</label></td>
		<td><input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo $getmoodle->password?>"></td>
	</tr>
		</tr>
		<tr>
		<td><label>Database Name</label></td>
		<td><input type="text" name="databasename" class="form-control" placeholder="Database Name" value="<?php echo $getmoodle->dbname; ?>"></td>
	</tr>
	<tr>
		<td colspan="2">
		    <button type="submit" class="btn btn-success pull-right" name="update" value="Update">Update</button>
		</td>
	</tr>

</table>
</form>
</hr>
</div>
</div>


<script src="//code.jquery.com/jquery-1.12.4.js"> </script>


<?php
						
include(ABSPATH . 'wp-admin/admin-footer.php');
						?>
