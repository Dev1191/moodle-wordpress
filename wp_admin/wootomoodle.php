<?php
//-----define( 'WP_DEBUG', true );
require_once(dirname(dirname(__FILE__)) . '/wp-load.php');
include(ABSPATH . 'wp-admin/admin-header.php');
auth_redirect();
$args     = array( 'numberposts' => -1,'post_type' => 'product', 'post_status' => 'publish',);
$products = get_posts( $args );
$getmoodle = $wpdb->get_row("SELECT * FROM wp_wooadminmoodle ORDER BY id DESC LIMIT 1 ");

$server = $getmoodle->server;
$username   = $getmoodle->username;
$password   = $getmoodle->password;
$database  = $getmoodle->dbname;


$con =  mysqli_connect($server,$username,$password,$database);
if (!$con) {
   echo "Failed to connect to MySQL: " . mysqli_connect_error();
} 

function getmoodlecoursename($courseid,$con){
 $result = mysqli_query($con,"select fullname from mdl_course WHERE id=$courseid"); 
 while($row1 = $result->fetch_assoc())
		{
return $row1['fullname'];
                }
}
//function getmoodlebundlename($bundle,$con){
// $result = mysqli_query($con,"select title from k5w_course_bundle WHERE id=$bundle"); 
// while($row1 = $result->fetch_assoc())
//		{
//return $row1['title'].'-bundle';
//                }
//}
$result = mysqli_query($con,"select mdl_course.id, mdl_course.fullname from mdl_course");
//$result1 = mysqli_query($con, "select DISTINCT(k5w_course_bundle.title), k5w_course_bundle.id as bundle_id from k5w_course_bundle");
if(isset($_REQUEST['save'])){
global $wpdb;
$cdate=date('Y-m-d h:i:s');
$product=$_REQUEST['product'];
$course1=$_REQUEST['course'];
if(strpos($course1,'b')){
  $course=(int)$course1;
  $bundle=1;
}else{
 $course=$_REQUEST['course']; 
 $bundle=0;
}

  $table_name = $wpdb->prefix . "wootomoodle";
  $wpdb->insert($table_name, array('courseid' => $course, 'productid' => $product,'bundle' => $bundle,'datecreated' =>$cdate) );  
}
//print_r($row);
//$arr = array_merge($row1, $row);
if(count($products)>0){
 ?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">
<style>
 #adminmenuwrap{
      height: 100% !important;
}
.previous {
    border-radius: 4px 0px 0px 4px;
    cursor: pointer;
    font-style: italic;
    text-decoration: none; 
    color: gray;
    margin-left: 10px;
    padding: 5px 20px;
    border: 1px solid #ccc;
}
.next {
    border-radius: 0px 4px 4px 0px;
    cursor: pointer;
    font-style: italic;
    text-decoration: none; 
    color: gray;
    margin-left: 10px;
    padding: 5px 20px;
    border: 1px solid #ccc;
}
#example_paginate span {
    
	cursor: pointer;
    color: #fff !important;
    padding: 6px 15px !important;
    margin-left: 10px !important;
}
</style>
<h3>Courses Mapping</h3>
<div class="grid">
<div class="row">
    <div class="col-md-6">
<form action="" name="moodleform">
  <div class="form-group">
    <label for="email">Woocommerce Product Name:</label>
   <select name="product" class="form-control">
    <?php
     foreach ($products as $product){
         ?> 
    <option value="<?php echo $product->ID ;?>"><?php echo $product->post_title ;?></option>
        <?php
     }
    ?>
</select>
  </div>
         <?php   
}
if ($result->num_rows > 0) {
    // output data of each row
	?>
  <div class="form-group">
    <label for="pwd">Moodle Course Name:</label>
    <select name="course" class="form-control">
	<?php
    while($row = $result->fetch_assoc()) {
       // echo "id: " . $row["id"]. "<br>";
	   ?>
	   <option value="<?php echo $row['id'] ;?>"><?php echo $row['fullname'] ;?></option>
           
	   <?php
	  // print_r($row);
    
    } ?>
	</select>
        
  </div>
    	<?php
} else {
    echo "0 results";
}?>
    <button type="submit" class="btn btn-success" name="save" value="Save">Submit</button>
</form>
</div>
</div></div>
<div class="divider"></div>
<h3>Courses Mapping List</h3>
<div class="row">
    <div class="col-md-8">
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>S No.</th>
                <th>WooCommerce Product Name</th>
                <th>Moodle Course Name</th>
                <th>Added Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>S No.</th>
                <th>WooCommerce Product Name</th>
                <th>Moodle Course Name</th>
                <th>Added Date</th>
                <th>Actions</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $myrows = $wpdb->get_results( "SELECT * FROM wp_wootomoodle");
            $i=1;			
 foreach ($myrows as $myrowsdata){
     $wooproduct = wc_get_product($myrowsdata->productid);


    ?>
        <tr>
                <td><?php echo $i.'.';?></td>
                <td><?php echo $wooproduct->get_title(); ?></td>
                <td><?php echo getmoodlecoursename($myrowsdata->courseid,$con); ?></td>
                <td><?php echo date('d/m/Y h:i:s',strtotime($myrowsdata->datecreated));?></td>
                <td><a class="btn btn-info" href="wootomoodleedit.php?id=<?php echo $myrowsdata->id ;?>">Edit</a>&nbsp;<a class="btn btn-danger" onclick="wootomoodledelete(<?php echo $myrowsdata->id ;?>)">Delete</a></td>
            </tr><?php 
$i++; }
            ?>
            
            
        </tbody>
</table></div></div>
<script src="//code.jquery.com/jquery-1.12.4.js"> </script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"> </script>
  <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
function wootomoodledelete(id){
var txt;
var r = confirm("Do you want to delete this record!");
if (r == true) {
    window.location.href="wootomoodledelete.php?id="+id;
} else {
   return false;
}
}
  </script>
    <?php 
						
						include(ABSPATH . 'wp-admin/admin-footer.php');
						?>
