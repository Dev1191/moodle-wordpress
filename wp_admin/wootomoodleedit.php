<?php
ob_start();
//define( 'WP_DEBUG', true );
require_once(dirname(dirname(__FILE__)) . '/wp-load.php');
include(ABSPATH . 'wp-admin/admin-header.php');
auth_redirect();
$args = array('numberposts' => -1, 'post_type' => 'product', 'post_status' => 'publish',);
$products = get_posts($args);
global $wpdb;

$getmoodle = $wpdb->get_row("SELECT * FROM wp_wooadminmoodle ORDER BY id DESC LIMIT 1 ");
$server = $getmoodle->server;
$username = $getmoodle->username;
$password = $getmoodle->password;
$database = $getmoodle->dbname;



$con = mysqli_connect($server, $username, $password, $database);
if (!$con) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


$result = mysqli_query($con, "SELECT mdl_course.id, mdl_course.fullname from mdl_course");
$myrows = $wpdb->get_row("SELECT * FROM wp_wootomoodle WHERE id='" . $_REQUEST['id'] . "'");

if (isset($_REQUEST['update'])) {

    $cdate = date('Y-m-d h:i:s');
    $id = $_REQUEST['mapid'];
    $product = $_REQUEST['product'];
    $course = $_REQUEST['course'];
//echo $id."=====================";

    $table_name = $wpdb->prefix . "wootomoodle";
    $wpdb->update($table_name, array('courseid' => $course, 'productid' => $product, 'datecreated' => $cdate), array('id' => $id), array('%s', '%s', '%s'), array('%s'));

    wp_redirect(admin_url() . 'wootomoodle.php');
}
//print_r($row);
if (count($products) > 0) {
    ?>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <style>
        #adminmenuwrap{
            height: 100% !important;
        }
    </style>

    <h3>Courses Mapping Edit</h3>
    <div class="grid">
        <div class="row">
            <div class="col-md-6">
                <form action="" name="moodleform">
                    <div class="form-group">
                        <label for="email">Woocommerce Product Name:</label>
                        <select name="product" class="form-control">
                            <?php
                            foreach ($products as $product) {
                                ?> 
                                <option <?php if ($product->ID == $myrows->productid) { ?> selected="selected"<?php } ?> value="<?php echo $product->ID; ?>"><?php echo $product->post_title; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <?php
                }
                if ($result->num_rows > 0) {
                    // output data of each row
                    ?>
                    <div class="form-group">
                        <label for="pwd">Moodle Course Name:</label>
                        <select name="course" class="form-control">
                            <?php
                            while ($row = $result->fetch_assoc()) {
                                // echo "id: " . $row["id"]. "<br>";
                                ?>
                                <option <?php if ($row['id'] == $myrows->courseid) { ?> selected="selected"<?php } ?> value="<?php echo $row['id']; ?>"><?php echo $row['fullname']; ?></option>

                                <?php
                                // print_r($row);
                            }
                            ?>
                        </select>
                        <input type="hidden" name="mapid" value="<?php echo $_REQUEST['id']; ?>">
                    </div>
                    <?php
                } else {
                    echo "0 results";
                }
                ?>
                <button type="submit" class="btn btn-success" name="update" value="update">Update</button>
            </form>
        </div>
    </div></div>

<?php
include(ABSPATH . 'wp-admin/admin-footer.php');
?>
